'use strict'
/**
 * BEGIN HEADER
 *
 * Contains:        File Utilities
 * CVM-Role:        <none>
 * Maintainer:      Matt Jolly
 * License:         GNU GPL v3
 *
 * Description:     This module contains file utilities that extend the default fs handling.
 *
 * END HEADER
 */

import chardet from 'chardet'
import iconv from 'iconv-lite'
import fs from 'fs'
import path from 'path'
import util from 'util'

const readFileAsync = util.promisify(fs.readFile)
const writeFileAsync = util.promisify(fs.writeFile)

/**
 * Extends default file handling to enable more encodings.
 *
 * @param     {string}  filePath  File that we're reading.
 * @returns   {string}            Usable file content.
 */
export async function readFileAsyncEncoding(filePath: string): Promise<string> {
  const content: Buffer = await readFileAsync(path.resolve(filePath))
  const encoding = chardet.detect(content)
  let decoded = ''
  if (typeof encoding === 'string') {
    decoded = iconv.decode(content, encoding)
  } else {
    throw new Error(`Could not detect endoding for file ${filePath}`)
  }
  return decoded
}

/**
 * A function that uses iconv-lite to convert a string to a buffer with a supported encoding and writes the buffer to disk.
 *
 * @param   {string}  filePath  The path that we want to write out to.
 * @param   {string}  content   The content that we want to convert and write.
 * @param   {string}  encoding  The endoding that we want to write out (iconv-supported values)
 */
export async function writeFileAsyncEncoding(filePath: string, content: string, encoding: string): Promise<void> {
  await writeFileAsync(path.resolve(filePath), iconv.encode(content, encoding))
}
